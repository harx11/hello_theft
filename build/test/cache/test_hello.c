#include "@@@@theft_hash.c"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_hash.c"
#include "@@@@theft_aux_builtin.c"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_aux_builtin.c"
#include "@@@@theft_aux.c"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_aux.c"
#include "build/temp/_test_hello.c"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_shrink.h"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_call.h"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_autoshrink.h"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_trial.h"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_bloom.h"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_random.h"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_rng.h"
#include "/proj/ciptmp/pi82tehe/theft/src/theft_run.h"
#include "/proj/ciptmp/pi82tehe/theft/inc/theft.h"
#include "test/add_Func.h"
#include "/proj/ciptmp/pi82tehe/Ceedling_Projects/hello_theft-2/vendor/ceedling/vendor/unity/src/unity.h"


;













void test_withCeedling() {

    int sum;

    sum = Addition(4,4);

    UnityAssertEqualNumber((UNITY_INT)((sum)), (UNITY_INT)((8)), (

   ((void *)0)

   ), (UNITY_UINT)(23), UNITY_DISPLAY_STYLE_INT);

}





 static enum theft_trial_res prop_addition(struct theft *t, void *arg1) {

        (void)t;

        int *num = (int *)arg1;

        int sum = Addition(*num, *num);

        return (sum ==*num+*num) ? THEFT_TRIAL_PASS : THEFT_TRIAL_FAIL;



}





void test_addition_pass (void) {

    theft_seed seed = theft_seed_of_time();



    struct theft_run_config cfg = {

        .name = __func__,

        .prop1 = prop_addition,

        .type_info = {

            theft_get_builtin_type_info(THEFT_BUILTIN_int),

        },



        .seed = seed,

        .trials = 10

    };



    UnityAssertEqualNumber((UNITY_INT)((THEFT_RUN_PASS)), (UNITY_INT)((theft_run(&cfg))), (

   ((void *)0)

   ), (UNITY_UINT)(50), UNITY_DISPLAY_STYLE_INT);

}
