#include "build/temp/_test_theft.c"
#include "test/test_theft.h"




static int greatest_name_match(const char *name, const char *filter) { size_t offset = 0; size_t filter_len = strlen(filter); while (name[offset] != '\0') { if (name[offset] == filter[0]) { if (0 == strncmp(&name[offset], filter, filter_len)) { return 1; } } offset++; } return 0; } int greatest_pre_test(const char *name) { if (!(greatest_info.flags & GREATEST_FLAG_LIST_ONLY) && (!(greatest_info.flags & GREATEST_FLAG_FIRST_FAIL) || greatest_info.suite.failed == 0) && (greatest_info.test_filter == 

((void *)0) 

|| greatest_name_match(name, greatest_info.test_filter))) { greatest_info.suite.pre_test = clock(); if (greatest_info.suite.pre_test == (clock_t) -1) { fprintf(

stdout

, "clock error: %s\n", "greatest_info.suite.pre_test"); exit(

1

); }; if (greatest_info.setup) { greatest_info.setup(greatest_info.setup_udata); } return 1; } else { return 0; } } void greatest_post_test(const char *name, int res) { greatest_info.suite.post_test = clock(); if (greatest_info.suite.post_test == (clock_t) -1) { fprintf(

stdout

, "clock error: %s\n", "greatest_info.suite.post_test"); exit(

1

); }; if (greatest_info.teardown) { void *udata = greatest_info.teardown_udata; greatest_info.teardown(udata); } if (res <= GREATEST_TEST_RES_FAIL) { greatest_do_fail(name); } else if (res >= GREATEST_TEST_RES_SKIP) { greatest_do_skip(name); } else if (res == GREATEST_TEST_RES_PASS) { greatest_do_pass(name); } greatest_info.suite.tests_run++; greatest_info.col++; if ((greatest_info.flags & GREATEST_FLAG_VERBOSE)) { fprintf(

stdout

, " (%lu ticks, %.3f sec)", (long unsigned int) (greatest_info.suite.post_test) - (long unsigned int)(greatest_info.suite.pre_test), (double)((greatest_info.suite.post_test) - (greatest_info.suite.pre_test)) / (1.0 * (double)

((__clock_t) 1000000)

)); fprintf(

stdout

, "\n"); } else if (greatest_info.col % greatest_info.width == 0) { fprintf(

stdout

, "\n"); greatest_info.col = 0; } if (

stdout 

== 

stdout

) fflush(

stdout

); } static void greatest_run_suite(greatest_suite_cb *suite_cb, const char *suite_name) { if (greatest_info.suite_filter && !greatest_name_match(suite_name, greatest_info.suite_filter)) { return; } if ((greatest_info.flags & GREATEST_FLAG_FIRST_FAIL) && greatest_info.failed > 0) { return; } memset(&greatest_info.suite, 0, sizeof(greatest_info.suite)); greatest_info.col = 0; fprintf(

stdout

, "\n* Suite %s:\n", suite_name); greatest_info.suite.pre_suite = clock(); if (greatest_info.suite.pre_suite == (clock_t) -1) { fprintf(

stdout

, "clock error: %s\n", "greatest_info.suite.pre_suite"); exit(

1

); }; suite_cb(); greatest_info.suite.post_suite = clock(); if (greatest_info.suite.post_suite == (clock_t) -1) { fprintf(

stdout

, "clock error: %s\n", "greatest_info.suite.post_suite"); exit(

1

); }; if (greatest_info.suite.tests_run > 0) { fprintf(

stdout

, "\n%u tests - %u pass, %u fail, %u skipped", greatest_info.suite.tests_run, greatest_info.suite.passed, greatest_info.suite.failed, greatest_info.suite.skipped); fprintf(

stdout

, " (%lu ticks, %.3f sec)", (long unsigned int) (greatest_info.suite.post_suite) - (long unsigned int)(greatest_info.suite.pre_suite), (double)((greatest_info.suite.post_suite) - (greatest_info.suite.pre_suite)) / (1.0 * (double)

((__clock_t) 1000000)

)); fprintf(

stdout

, "\n"); } greatest_info.setup = 

((void *)0)

; greatest_info.setup_udata = 

((void *)0)

; greatest_info.teardown = 

((void *)0)

; greatest_info.teardown_udata = 

((void *)0)

; greatest_info.passed += greatest_info.suite.passed; greatest_info.failed += greatest_info.suite.failed; greatest_info.skipped += greatest_info.suite.skipped; greatest_info.tests_run += greatest_info.suite.tests_run; } void greatest_do_pass(const char *name) { if ((greatest_info.flags & GREATEST_FLAG_VERBOSE)) { fprintf(

stdout

, "PASS %s: %s", name, greatest_info.msg ? greatest_info.msg : ""); } else { fprintf(

stdout

, "."); } greatest_info.suite.passed++; } void greatest_do_fail(const char *name) { if ((greatest_info.flags & GREATEST_FLAG_VERBOSE)) { fprintf(

stdout

, "FAIL %s: %s (%s:%u)", name, greatest_info.msg ? greatest_info.msg : "", greatest_info.fail_file, greatest_info.fail_line); } else { fprintf(

stdout

, "F"); greatest_info.col++; if (greatest_info.col != 0) { fprintf(

stdout

, "\n"); greatest_info.col = 0; } fprintf(

stdout

, "FAIL %s: %s (%s:%u)\n", name, greatest_info.msg ? greatest_info.msg : "", greatest_info.fail_file, greatest_info.fail_line); } greatest_info.suite.failed++; } void greatest_do_skip(const char *name) { if ((greatest_info.flags & GREATEST_FLAG_VERBOSE)) { fprintf(

stdout

, "SKIP %s: %s", name, greatest_info.msg ? greatest_info.msg : "" ); } else { fprintf(

stdout

, "s"); } greatest_info.suite.skipped++; } int greatest_do_assert_equal_t(const void *exp, const void *got, greatest_type_info *type_info, void *udata) { int eq = 0; if (type_info == 

((void *)0) 

|| type_info->equal == 

((void *)0)

) { return 0; } eq = type_info->equal(exp, got, udata); if (!eq) { if (type_info->print != 

((void *)0)

) { fprintf(

stdout

, "\nExpected: "); (void)type_info->print(exp, udata); fprintf(

stdout

, "\nGot: "); (void)type_info->print(got, udata); fprintf(

stdout

, "\n"); } else { fprintf(

stdout

, "GREATEST_ASSERT_EQUAL_T failure at %s:%dn", greatest_info.fail_file, greatest_info.fail_line); } } return eq; } void greatest_usage(const char *name) { fprintf(

stdout

, "Usage: %s [-hlfv] [-s SUITE] [-t TEST]\n" "  -h        print this Help\n" "  -l        List suites and their tests, then exit\n" "  -f        Stop runner after first failure\n" "  -v        Verbose output\n" "  -s SUITE  only run suite named SUITE\n" "  -t TEST   only run test named TEST\n", name); } int greatest_all_passed() { return (greatest_info.failed == 0); } void GREATEST_SET_SETUP_CB(greatest_setup_cb *cb, void *udata) { greatest_info.setup = cb; greatest_info.setup_udata = udata; } void GREATEST_SET_TEARDOWN_CB(greatest_teardown_cb *cb, void *udata) { greatest_info.teardown = cb; greatest_info.teardown_udata = udata; } static int greatest_string_equal_cb(const void *exp, const void *got, void *udata) { (void)udata; return (0 == strcmp((const char *)exp, (const char *)got)); } static int greatest_string_printf_cb(const void *t, void *udata) { (void)udata; return fprintf(

stdout

, "%s", (const char *)t); } greatest_type_info greatest_type_info_string = { greatest_string_equal_cb, greatest_string_printf_cb, }; greatest_run_info greatest_info;



int main(int argc, char **argv) {

    do { int i = 0; do { memset(&greatest_info, 0, sizeof(greatest_info)); greatest_info.width = 72; greatest_info.begin = clock(); if (greatest_info.begin == (clock_t) -1) { fprintf(

   stdout

   , "clock error: %s\n", "greatest_info.begin"); exit(

   1

   ); }; } while (0); for (i = 1; i < argc; i++) { if (0 == strcmp("-t", argv[i])) { if (argc <= i + 1) { greatest_usage(argv[0]); exit(

   1

   ); } greatest_info.test_filter = argv[i+1]; i++; } else if (0 == strcmp("-s", argv[i])) { if (argc <= i + 1) { greatest_usage(argv[0]); exit(

   1

   ); } greatest_info.suite_filter = argv[i+1]; i++; } else if (0 == strcmp("-f", argv[i])) { greatest_info.flags |= GREATEST_FLAG_FIRST_FAIL; } else if (0 == strcmp("-v", argv[i])) { greatest_info.flags |= GREATEST_FLAG_VERBOSE; } else if (0 == strcmp("-l", argv[i])) { greatest_info.flags |= GREATEST_FLAG_LIST_ONLY; } else if (0 == strcmp("-h", argv[i])) { greatest_usage(argv[0]); exit(

   0

   ); } else { fprintf(

   stdout

   , "Unknown argument '%s'\n", argv[i]); greatest_usage(argv[0]); exit(

   1

   ); } } } while (0);

    greatest_run_suite(Addition, "Addition");

    do { do { if (!(greatest_info.flags & GREATEST_FLAG_LIST_ONLY)) { greatest_info.end = clock(); if (greatest_info.end == (clock_t) -1) { fprintf(

   stdout

   , "clock error: %s\n", "greatest_info.end"); exit(

   1

   ); }; fprintf(

   stdout

   , "\nTotal: %u tests", greatest_info.tests_run); fprintf(

   stdout

   , " (%lu ticks, %.3f sec)", (long unsigned int) (greatest_info.end) - (long unsigned int)(greatest_info.begin), (double)((greatest_info.end) - (greatest_info.begin)) / (1.0 * (double)

   ((__clock_t) 1000000)

   )); fprintf(

   stdout

   , ", %u assertions\n", greatest_info.assertions); fprintf(

   stdout

   , "Pass: %u, fail: %u, skip: %u.\n", greatest_info.passed, greatest_info.failed, greatest_info.skipped); } } while (0); return (greatest_all_passed() ? 

   0 

   : 

   1

   ); } while (0);

}
