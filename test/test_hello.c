#include "unity.h"
#include "add_Func.h"
#include "theft.h"
#include "theft_run.h"
#include "theft_rng.h"
#include "theft_random.h"
#include "theft_bloom.h"
#include "theft_trial.h"
#include "theft_autoshrink.h"
#include "theft_call.h"
#include "theft_shrink.h"

TEST_FILE("theft_aux.c");
TEST_FILE("theft_aux_builtin.c")
TEST_FILE("theft_hash.c")



//works  with 'ceedling test:all' command
void test_withCeedling() {
    int sum;
    sum = Addition(4,4);  //addition is defined in add_Func.h
    TEST_ASSERT_EQUAL(sum,8);  //(expected value, desired value)
}

/** THEFT -  DEFINE PROPERTY **/ 
 static enum theft_trial_res prop_addition(struct theft *t, void *arg1) {
        (void)t;
        int *num = (int *)arg1;
        int sum = Addition(*num, *num);
        return (sum ==*num+*num) ? THEFT_TRIAL_PASS : THEFT_TRIAL_FAIL;
        
}

/* THEFT-  DEFINE TEST CONFIG*/
void test_addition_pass (void) {
    theft_seed seed = theft_seed_of_time();

    struct theft_run_config cfg = {
        .name = __func__,
        .prop1 = prop_addition,
        .type_info = {
            theft_get_builtin_type_info(THEFT_BUILTIN_int),
        },
        //.bloom_bits = 20,
        .seed = seed,
        .trials = 10
    };

    TEST_ASSERT_EQUAL(THEFT_RUN_PASS, theft_run(&cfg));
}

